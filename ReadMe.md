$ roslaunch usb_cam usb_cam-test.launch  
$ rosrun image_view image_view image:=/usb_cam/image_raw  
$ rosrun image_view image_saver  __name:=image_saver image:=/usb_cam/image_raw _save_all_image:=false  
$ rosrun image_view image_saver  __name:=image_saver image:=/usb_cam/image_raw _save_all_image:=false _filename_format:=foo%02d.jpg  


# Weiss Tactile Sensors - ROS Driver
This package provides a ROS interface/driver to the Weiss Tactile Sensors.

## To run the driver for one sensor with modified src please run
$ rosrun wts_driver wts_driver_node /dev/ttyACM0

## To run the driver for one sensor please run
$ roslaunch wts_driver wts_one_sensor.launch

## To run the driver for two sensors please run
$ roslaunch wts_driver wts_two_sensors.launch

## Check the USB-Camera information
$ v4l2-ctl -d /dev/video0 --all

## Camera calibration
$ rosrun camera_calibration cameracalibrator.py --size 8x6 --square 0.0225 image:=/usb_cam/image_raw camera:=/usb_cam


# Template Search and Foam Track
$ roslaunch data_collect_pkg usb_cam.launch  
$ roslaunch data_collect_pkg tplsearch.launch  
$ roslaunch data_collect_pkg tracker.launch
