#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 14:02:26 2018

@author: ga94caf
"""
import sys

import rospy
from rospy_message_converter import json_message_converter

from sensor_msgs.msg import Image

if __name__ == '__main__':
    rospy.init_node('test_node')
    rospy.loginfo('***test_node initialized......')
    
    pub_image = rospy.Publisher('test_image', Image, queue_size=10)


    data_str = open('/home/ga94caf/ros_workspaces/fp_ws/src/data_collect_pkg/data/021_solevitaapfel_closed_empty0.json').read()
    
    msg = json_message_converter.convert_json_to_ros_message('data_collect_pkg/Sample', data_str)
    
    while True:
        
        num_location = len(msg.pressure_distribution)
        num_image = len(msg.pressure_distribution[0].image)

        for i in range(num_location):
            print('--> location %d:' %(i+1))

            for j in range(num_image):

                sys.stdout.write('F%02d: %5.2fN    ' % (j+1, msg.pressure_distribution[i].force[j] ))
                sys.stdout.flush()
                if j%4 == 3:
                    print 
                print 

                count = 0
                r = rospy.Rate(50) # 50hz
                while True:
                    count += 1
                    pub_image.publish(msg.pressure_distribution[i].image[j])
                    
                    if count == 150:
                        count = 0
                        break
                    
                    r.sleep()


