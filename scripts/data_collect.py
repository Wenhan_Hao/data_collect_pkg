#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 10:31:38 2018

@author: ga94caf
"""
import sys

#import roslib
import rospy
#import time
import numpy as np

from rospy_message_converter import json_message_converter

#import std_msgs.msg
from std_msgs.msg import Int32MultiArray, Header
from sensor_msgs.msg import Image
from geometry_msgs.msg import *

from data_collect_pkg.msg import Data, Sample, PressureDistribution
from data_collect_pkg.srv import Grasp, SetGripperLocation, SetGripperLocationRequest
from wsg_50_common.msg import Cmd, Status



class data_collect():
    camera_raw = Image()
    tactile_pressure = Int32MultiArray()
    gripper = Status()
    gripper_force = [0.0] * 100
    
    
    #FORCE = np.arange(1.0, 15.0, 0.3)
    FORCE = [1.00, 1.30, 1.65, 2.05, 2.50, 3.00, 3.55, 4.15, 4.80, 5.50, 6.25, 7.05, 7.90, 8.80, 9.75, 10.75, 11.80, 12.90, 14.05, 15.25]
    LOCATION_NUMBER = 9
    SAMPLE_NUMBER = 3
    GRIPPER_BROAD_HEIGHT = 0.034
    DATA_PATH = '/home/ga94caf/ros_workspaces/fp_ws/src/data_collect_pkg/data/'
    
    
    def __init__(self):
        self.sub_tactile = rospy.Subscriber('/tactile_pressure', Int32MultiArray, self.cb_tactile)

        self.sub_image = rospy.Subscriber('/usb_cam/image_rect', Image, self.cb_camera)
        
        self.sub_gripper = rospy.Subscriber('/wsg_50_driver/status', Status, self.cb_gripper)

        self.pub_gripper_open = rospy.Publisher('/wsg_50_driver/goal_position', Cmd, queue_size=10)

        self.client_gripper_loc = rospy.ServiceProxy('/set_gripper_location', SetGripperLocation)

        self.client_grasp = rospy.ServiceProxy('/grasp_srv', Grasp)
                
        self.excute()
        
                                          
    def cb_tactile(self, msg):
        self.tactile_pressure = msg
    
    def cb_camera(self, msg):
        self.camera_raw = msg
        
    def cb_gripper(self, msg):
        self.gripper = msg
        self.gripper_force.pop(0)
        self.gripper_force.append(msg.force_finger1)

    def excute(self):
        print('--------------------------------------------------------------------------------')
        print('****************************** GORGEOUS SPLIT LINE  ****************************')
        print('--------------------------------------------------------------------------------')
        print('Starting to collect data ......')
        command = raw_input('Press [Y/y] to continue, any other key to exit: ')
        
        if command != 'Y' and command != 'y':
            return

        # Open the gripper to maximal position
        self.pub_gripper_open.publish(pos = 108, speed = 20)

        while True:
            print('===============================================================================')
            sample = Sample()
            
            raw_input('==> Please put object in right location, and press any key to continue: ')
            
            sample.label = input('==> Input the label(This should be a number): ')
            sample.name = raw_input('==> Input the object name: ')
            sample.openif = input('==> Is the object open? [True/False]: ')
            sample.height = input('==> Input the object height[/m] : ')
            sample.width  = input('==> Input the object width[/m] : ')

            sample.location = list(np.linspace(start = self.GRIPPER_BROAD_HEIGHT/2 + 0.01,
                                               stop = sample.height - self.GRIPPER_BROAD_HEIGHT/2 - 0.01,
                                               endpoint = True,
                                               num = self.LOCATION_NUMBER))
            #sample.location = list(np.linspace(0.027, sample.height-0.027, num=self.LOCATION_NUMBER))
            
            for k in range(self.SAMPLE_NUMBER):
                print('-----------------------------%s: %d----------------------------' % (sample.name, k+1))
                for i,u in enumerate(sample.location):
                    # Create a varable to save the temporary data
                    pressure_distribution = PressureDistribution()
                    
                    
                    # Move the gripper to location[i]                 
                    self.client_gripper_loc(PoseStamped(header=Header(frame_id='base_link'),
                                                        pose=Pose(position=Point(x=-0.56, y=0.415, z=0.127+u),
                                                                  orientation=Quaternion(x=0.585, y=-0.4, z=-0.41, w=0.60))))
                    print('--> location %d:' %(i+1))
                    
                    for j, v in enumerate(self.FORCE):
                        # Grasp
                        self.client_grasp(force=v)
                        
                        # wait until the force sensor is stable
                        force_var = np.array(self.gripper_force).var()
                        while force_var >= 0.05:
                            force_var = np.array(self.gripper_force).var()

                        # record the force and gripper opening width
                        pressure_distribution.force.append(np.array(self.gripper_force).mean())
                        pressure_distribution.wsg_width.append(self.gripper.width)
                        
                        # Collect image and pressure
                        pressure_distribution.image.append(self.camera_raw)
                        pressure_distribution.pressure.append(self.tactile_pressure)
                        
                        # print the information to terminal
                        sys.stdout.write('F%02d: %5.2fN    ' % (j+1, np.array(self.gripper_force).mean() ))
                        sys.stdout.flush()
                        if j%4 == 3:
                            print 

                    print 
                    # Save the information of location[i]
                    sample.pressure_distribution.append(pressure_distribution)
                    while len(sample.pressure_distribution) > self.LOCATION_NUMBER:
                        sample.pressure_distribution.pop(0)

                    # Open gripper, everytime maximal 20mm
                    for i in range(4):
                        GRIPPER_OPEN_temp = min(108, sample.width*1000 + 40, self.gripper.width + 20)
                        
                        self.pub_gripper_open.publish(pos = GRIPPER_OPEN_temp, speed = 5)
                        # wait for action to end
                        rospy.sleep((GRIPPER_OPEN_temp - self.gripper.width)/5)

                            
                # Convert ros message type to json format
                sample_json = json_message_converter.convert_ros_message_to_json(sample)
                
                # Get the timestamp
                #time_str = time.strftime('%Y%m%d_%H%M%S', time.localtime())
                
                # Save the data in json format, like 20180131_135530.json
                with open(self.DATA_PATH + str(sample.label).zfill(3) + '_' + sample.name + str(k) +'.json', 'w') as f:
                    f.write(sample_json)
                print(str(sample.label).zfill(3) + '_' + sample.name + str(k) +'.json' + ' was saved in ' + self.DATA_PATH)
            
            print('\n\nDo you want to continue?')
            command = raw_input('Press [Y/y] to continue, any other key to exit: ')
            if command != 'Y' and command != 'y':
                break

if __name__ == '__main__':
    rospy.init_node('data_collect_node')
    rospy.loginfo('***data_collect_node initialized......')

    dc = data_collect()
    
    #rospy.spin()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
